/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.sql;

import com.gluonapplicationdemo.model.Klanovi;
import com.gluonapplicationdemo.model.Kursevi;
import com.gluonapplicationdemo.model.Nindza;
import com.gluonapplicationdemo.model.OglasnaTabla;
import com.gluonapplicationdemo.model.RasporedPredavanja;
import static com.gluonapplicationdemo.util.CryptoMaster.decrypt;
import static com.gluonapplicationdemo.util.CryptoMaster.encrypt;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;

/**
 *
 * @author Grupa
 */
public class sqltools {

    private static final String DB_CONNECTION = "jdbc:mysql://194.135.89.121:3306/ngonga";
    private static final String DB_USER = "stipe";
    private static final String DB_PASSWORD = "ngonga daje savete";

    public static Connection connection = null;
    public static Nindza kornjaca = null;
    public static Map<Integer, String> mapaNecega = null;

    public static Connection getDBConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            System.out.println("Connection ok");
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("CONNECTION ERROR...");
            alertMessage(e.toString());
        }
        return connection;
    }

    public static void alertMessage(String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Information");
        alert.setContentText("Sadrzaj : " + msg);
        alert.showAndWait();
    }

    public static Map<Integer, String> mapiraj(String nazivTabele) {
        mapaNecega = new HashMap<>();
        try (Connection con = getDBConnection()) {
            String query = "select * from " + nazivTabele;
            try (PreparedStatement prest = con.prepareStatement(query)) {
                ResultSet result = prest.executeQuery();
                while (result.next()) {
                    mapaNecega.put(result.getInt(1), result.getString(2));
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mapaNecega;
    }

    public static Integer vratiIdIzMape(String nazivTabele, String vrednost) {
        Integer kljuc = null;
        mapaNecega = mapiraj(nazivTabele);
        for (int key : mapaNecega.keySet()) {
            if (mapaNecega.get(key).equals(vrednost)) {
                kljuc = key;
            }
        }
        return kljuc;
    }

    public static Integer dodajNindzu(String ime, String prezime, String email, String telefon, String idBrStudenta, String sifra) {
        byte[] sifretina = encrypt(sifra, email);

        int i = -1;
        try {
            Connection con = getDBConnection();
            String query = "insert into nindza(ime, prezime, email, telefon, idBrStudenta, pass, nivo, klanNindze) values(?,?,?,?,?,?,?,?)";
            PreparedStatement prest = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            prest.setString(1, ime);
            prest.setString(2, prezime);
            prest.setString(3, email);
            prest.setString(4, telefon);
            prest.setString(5, idBrStudenta);
            prest.setBytes(6, sifretina);
            prest.setInt(7, 0);
            prest.setInt(8, 1);
            prest.executeUpdate();
            ResultSet rs = prest.getGeneratedKeys();
            if (rs.next()) {
                System.out.println("postoji rezultat");
                i = rs.getInt(1);
                System.out.println("i = " + i);
            } else {
                System.out.println("ne postoji resultset");
            }
            prest.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("1");
        } finally {
            System.out.println("Profil uspesno dodat!");
            return i;
        }
    }

    public static boolean nindzaExists(String email) {
        boolean res = false;
        try {
            Connection con = getDBConnection();
            String query
                    = "select * "
                    + "from nindza "
                    + "where email = ?";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, email);

            ResultSet result = st.executeQuery();
            if (result.next()) {
                res = true;
            }
            st.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return res;
        }
    }

    public static String vratiSifruIzBaze(String email) {
        try {
            String sifra = "";
            Connection con = getDBConnection();
            String query
                    = "select * "
                    + "from nindza "
                    + "where email = ?";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, email);
            ResultSet result = st.executeQuery();
            if (result.next()) {

                Blob passBlob = result.getBlob("pass");
                int blobLength = (int) passBlob.length();
                byte[] temPass = passBlob.getBytes(1, blobLength);
                //release the blob and free up memory. (since JDBC 4.0)
                passBlob.free();
                sifra = decrypt(temPass, email);
            }

            st.close();
            con.close();
            return sifra;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<OglasnaTabla> skupiPorukeZaTablu() {
        try {
            ArrayList<OglasnaTabla> spisakPoruka = new ArrayList<>();
            Connection con = getDBConnection();
            String query
                    = "select * "
                    + "from oglasnaTabla "
                    + "order by vremePoruke";
            PreparedStatement st = con.prepareStatement(query);

            ResultSet result = st.executeQuery();
            while (result.next()) {
                int idPoruke = result.getInt("id_oglasnaTabla");
                int idNindze = result.getInt("idNindze");
                String tekstPoruke = result.getString("tekstPoruke");
                Date datumPorukeIzBaze = result.getTimestamp("vremePoruke");
                spisakPoruka.add(new OglasnaTabla(idPoruke, idNindze, tekstPoruke, datumPorukeIzBaze));
            }
            st.close();
            con.close();

            return spisakPoruka;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void dodajPoruku(int idNindze, String tekst) {
        try {
            try (Connection con = getDBConnection()) {
                String query = "insert into oglasnaTabla(idNindze,tekstPoruke,vremePoruke) values(?,?,?)";
                try (PreparedStatement prest = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                    prest.setInt(1, idNindze);
                    prest.setString(2, tekst);
                    prest.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                    prest.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Nindza vratiNindzuPoId(Integer idNindze) {
        try {
            Nindza kornjace = null;
            Connection con = getDBConnection();
            String query
                    = "select * "
                    + "from nindza "
                    + "where id_nindze = ?";
            PreparedStatement st = con.prepareStatement(query);
            st.setInt(1, idNindze);
            ResultSet result = st.executeQuery();
            if (result.next()) {
                System.out.println("Ime: " + result.getString("ime"));
                kornjace = new Nindza();
                kornjace.setIme(result.getString("ime"));
                kornjace.setPrezime(result.getString("prezime"));
                kornjace.setEmail(result.getString("email"));
            }
            st.close();
            con.close();
            return kornjace;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Nindza vratiNindzuPoImenu(String email) {
        try {
            try (Connection con = getDBConnection()) {
                String query
                        = "select * "
                        + "from nindza "
                        + "where email = ?";
                PreparedStatement st = con.prepareStatement(query);
                st.setString(1, email);
                ResultSet result = st.executeQuery();
                while (result.next()) {
                    kornjaca = new Nindza();
                    kornjaca.setIdNindze(result.getInt("id_nindze"));
                    kornjaca.setIme(result.getString("ime"));
                    kornjaca.setPrezime(result.getString("prezime"));
                    kornjaca.setKlanNindze(result.getInt("klanNindze"));
                    kornjaca.setNivo(result.getInt("nivo"));
                    if (!result.getString("idBrStudenta").isEmpty()) {
                        kornjaca.setIdBrStudenta(result.getString("idBrStudenta"));
                    } else {
                        kornjaca.setIdBrStudenta("/");
                    }
                }
                st.close();
            }
            return kornjaca;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<Klanovi> napraviListuKlanova() {
        try {
            ArrayList<Klanovi> listaKlanova = new ArrayList<>();
            try (Connection con = getDBConnection()) {
                String query
                        = "select * "
                        + "from klanovi";
                try (PreparedStatement st = con.prepareStatement(query)) {
                    ResultSet result = st.executeQuery();
                    while (result.next()) {
                        int idKlana = result.getInt("idKlana");
                        String nazivKlana = result.getString("nazivKlana");

                        listaKlanova.add(new Klanovi(idKlana, nazivKlana));
                    }
                }
            }

            return listaKlanova;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<Kursevi> napraviListuKurseva() {
        try {
            ArrayList<Kursevi> listaKurseva = new ArrayList<>();
            try (Connection con = getDBConnection()) {
                String query
                        = "select * "
                        + "from kursevi "
                        + "order by idKursevi";
                try (PreparedStatement prest = con.prepareStatement(query)) {
                    ResultSet result = prest.executeQuery();
                    while (result.next()) {
                        int idKursa = result.getInt("idKursevi");
                        String nazivKursa = result.getString("nazivKursa");

                        listaKurseva.add(new Kursevi(idKursa, nazivKursa));
                    }
                }
            }

            return listaKurseva;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String vratiNazivKlanaPoId(Integer id) {
        String nazivKlana = null;
        try (Connection con = getDBConnection()) {
            String query
                    = "select * "
                    + "from klanovi "
                    + "where idKlana = ?";
            try (PreparedStatement st = con.prepareStatement(query)) {
                st.setInt(1, id);

                ResultSet result = st.executeQuery();
                if (result.next()) {
                    nazivKlana = result.getString("nazivKlana");
                }
            }
            return nazivKlana;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ArrayList<RasporedPredavanja> napraviRasporedPredavanja() {
        ArrayList<RasporedPredavanja> raspored = new ArrayList<>();
        try {
            try (Connection con = getDBConnection()) {
                String query
                        = "select * "
                        + "from rasporedPredavanja";
                try (PreparedStatement prest = con.prepareStatement(query)) {
                    ResultSet result = prest.executeQuery();
                    while (result.next()) {
                        RasporedPredavanja predavanje = new RasporedPredavanja();
                        predavanje.setNazivPredavanja(result.getString("nazivPredavanja"));
                        predavanje.setKurs(result.getInt("kurs"));
                        predavanje.setKlan(result.getInt("klan"));
                        predavanje.setPredavac(result.getInt("predavac"));
                        predavanje.setMaterijal(result.getInt("materijal"));
                        predavanje.setPocetakPred(result.getTimestamp("pocetakPred"));
                        predavanje.setKrajPredavanja(result.getTimestamp("krajPredavanja"));

                        raspored.add(predavanje);
                    }
                }
                con.close();
            }
            return raspored;
        } catch (SQLException ex) {
            Logger.getLogger(sqltools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
