package com.gluonapplicationdemo;

import static com.gluonapplicationdemo.GluonApplicationDemo.FIFTH_VIEW;
import static com.gluonapplicationdemo.GluonApplicationDemo.FOURTH_VIEW;
import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.LifecycleService;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.application.ViewStackPolicy;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.control.NavigationDrawer.Item;
import com.gluonhq.charm.glisten.control.NavigationDrawer.ViewItem;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import static com.gluonapplicationdemo.GluonApplicationDemo.PRIMARY_VIEW;
import javafx.scene.image.Image;
import static com.gluonapplicationdemo.GluonApplicationDemo.SECOND_VIEW;
import static com.gluonapplicationdemo.GluonApplicationDemo.SEVENTH_VIEW;
import static com.gluonapplicationdemo.GluonApplicationDemo.SIXTH_VIEW;
import static com.gluonapplicationdemo.GluonApplicationDemo.THIRD_VIEW;

public class DrawerManager {

    public static NavigationDrawer drawer;
    public static NavigationDrawer.Header header;

    public static void buildDrawer(MobileApplication app) {
        drawer = app.getDrawer();

        String imeStudentaString = "Test Student";
        String razredStudenta = "Test Razred";
        Avatar glavic = new Avatar(21, new Image(DrawerManager.class.getResourceAsStream("/profile-placeholder.png")));

        header = new NavigationDrawer.Header(imeStudentaString,razredStudenta, glavic);
        drawer.setHeader(header);
        header.setOnAction(e -> {
            MobileApplication.getInstance().switchView(SECOND_VIEW, ViewStackPolicy.SKIP);
            drawer.close();
        });

        final Item loginItem = new ViewItem("Login", MaterialDesignIcon.REFRESH.graphic(), PRIMARY_VIEW);
        final Item glavniItem = new ViewItem("Glavna", MaterialDesignIcon.HOME.graphic(), SECOND_VIEW);
        final Item profilItem = new ViewItem("Profil", MaterialDesignIcon.ACCOUNT_CIRCLE.graphic(), THIRD_VIEW);
        final Item rasporedItem = new ViewItem("Raspored", MaterialDesignIcon.SCHEDULE.graphic(), FOURTH_VIEW);
        final Item kurseviItem = new ViewItem("Kursevi", MaterialDesignIcon.SUBSCRIPTIONS.graphic(), FIFTH_VIEW);
        final Item materijalItem = new ViewItem("Materijali", MaterialDesignIcon.BOOK.graphic(), SIXTH_VIEW);
        final Item testoviItem = new ViewItem("Testovi", MaterialDesignIcon.CHECK_BOX.graphic(), SEVENTH_VIEW);

        drawer.getItems().addAll(glavniItem, profilItem, rasporedItem, kurseviItem, materijalItem, testoviItem, loginItem);

        drawer.setStyle("-fx-background-color: gray;");

        if (Platform.isDesktop()) {
            final Item quitItem = new Item("Quit", MaterialDesignIcon.EXIT_TO_APP.graphic());
            quitItem.selectedProperty().addListener((obs, ov, nv) -> {
                if (nv) {
                    Services.get(LifecycleService.class).ifPresent(LifecycleService::shutdown);
                }
            });
            drawer.getItems().add(quitItem);
        }
    }
}
