package com.gluonapplicationdemo;

import static com.gluonapplicationdemo.sql.sqltools.kornjaca;
import static com.gluonapplicationdemo.sql.sqltools.vratiNindzuPoImenu;
import com.gluonapplicationdemo.views.KurseviView;
import com.gluonapplicationdemo.views.RasporedView;
import com.gluonapplicationdemo.views.ProfilView;
import com.gluonapplicationdemo.views.MaterijaliView;
import com.gluonapplicationdemo.views.GlavniView;
import com.gluonapplicationdemo.views.LoginView;
import static com.gluonapplicationdemo.views.LoginView.imeNindze;
import static com.gluonapplicationdemo.views.LoginView.templista;
import com.gluonapplicationdemo.views.TestoviView;
import com.gluonhq.charm.down.Platform;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.application.ViewStackPolicy;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import static com.gluonapplicationdemo.views.ProfilView.ucitajGlavic;

public class GluonApplicationDemo extends MobileApplication {

    public static final String PRIMARY_VIEW = HOME_VIEW;
    public static final String SECOND_VIEW = "LoginView";
    public static final String THIRD_VIEW = "ProfilTest";
    public static final String FOURTH_VIEW = "RasporedTest";
    public static final String FIFTH_VIEW = "KurseviTest";
    public static final String SIXTH_VIEW = "MaterijaliTest";
    public static final String SEVENTH_VIEW = "TestoviTest";

    @Override
    public void init() {

        addViewFactory(THIRD_VIEW, ProfilView::new);
        addViewFactory(PRIMARY_VIEW, LoginView::new);
        addViewFactory(SECOND_VIEW, GlavniView::new);
        addViewFactory(FOURTH_VIEW, RasporedView::new);
        addViewFactory(FIFTH_VIEW, KurseviView::new);
        addViewFactory(SIXTH_VIEW, MaterijaliView::new);
        addViewFactory(SEVENTH_VIEW, TestoviView::new);

        DrawerManager.buildDrawer(this);

    }

    @Override
    public void postInit(Scene scene) {
        Swatch.GREY.assignTo(scene);

        scene.getStylesheets().add(GluonApplicationDemo.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(GluonApplicationDemo.class.getResourceAsStream("/slovoG.png")));
        if (LoginView.proveraCookija()) {
            ucitajGlavic(kornjaca);
            MobileApplication.getInstance().switchView(SECOND_VIEW, ViewStackPolicy.SKIP);
        }
    }
}
