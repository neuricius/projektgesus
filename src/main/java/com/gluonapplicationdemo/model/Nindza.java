/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "nindza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nindza.findAll", query = "SELECT n FROM Nindza n")
    , @NamedQuery(name = "Nindza.findByIdNindze", query = "SELECT n FROM Nindza n WHERE n.idNindze = :idNindze")
    , @NamedQuery(name = "Nindza.findByIme", query = "SELECT n FROM Nindza n WHERE n.ime = :ime")
    , @NamedQuery(name = "Nindza.findByPrezime", query = "SELECT n FROM Nindza n WHERE n.prezime = :prezime")
    , @NamedQuery(name = "Nindza.findByEmail", query = "SELECT n FROM Nindza n WHERE n.email = :email")
    , @NamedQuery(name = "Nindza.findByTelefon", query = "SELECT n FROM Nindza n WHERE n.telefon = :telefon")
    , @NamedQuery(name = "Nindza.findByIdBrStudenta", query = "SELECT n FROM Nindza n WHERE n.idBrStudenta = :idBrStudenta")
    , @NamedQuery(name = "Nindza.findByNivo", query = "SELECT n FROM Nindza n WHERE n.nivo = :nivo")
    , @NamedQuery(name = "Nindza.findByKlanNindze", query = "SELECT n FROM Nindza n WHERE n.klanNindze = :klanNindze")})
public class Nindza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nindze")
    private Integer idNindze;
    @Column(name = "ime")
    private String ime;
    @Column(name = "prezime")
    private String prezime;
    @Column(name = "email")
    private String email;
    @Column(name = "telefon")
    private String telefon;
    @Column(name = "idBrStudenta")
    private String idBrStudenta;
    @Basic(optional = false)
    @Lob
    @Column(name = "pass")
    private byte[] pass;
    @Basic(optional = false)
    @Column(name = "nivo")
    private int nivo;
    @Basic(optional = false)
    @Column(name = "klanNindze")
    private int klanNindze;
    @Lob
    @Column(name = "liceNindze")
    private byte[] liceNindze;

    public Nindza() {
    }

    public Nindza(Integer idNindze) {
        this.idNindze = idNindze;
    }

    public Nindza(Integer idNindze, byte[] pass, int nivo, int klanNindze) {
        this.idNindze = idNindze;
        this.pass = pass;
        this.nivo = nivo;
        this.klanNindze = klanNindze;
    }

    public Integer getIdNindze() {
        return idNindze;
    }

    public void setIdNindze(Integer idNindze) {
        this.idNindze = idNindze;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getIdBrStudenta() {
        return idBrStudenta;
    }

    public void setIdBrStudenta(String idBrStudenta) {
        this.idBrStudenta = idBrStudenta;
    }

    public byte[] getPass() {
        return pass;
    }

    public void setPass(byte[] pass) {
        this.pass = pass;
    }

    public int getNivo() {
        return nivo;
    }

    public void setNivo(int nivo) {
        this.nivo = nivo;
    }

    public int getKlanNindze() {
        return klanNindze;
    }

    public void setKlanNindze(int klanNindze) {
        this.klanNindze = klanNindze;
    }

    public byte[] getLiceNindze() {
        return liceNindze;
    }

    public void setLiceNindze(byte[] liceNindze) {
        this.liceNindze = liceNindze;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNindze != null ? idNindze.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nindza)) {
            return false;
        }
        Nindza other = (Nindza) object;
        if ((this.idNindze == null && other.idNindze != null) || (this.idNindze != null && !this.idNindze.equals(other.idNindze))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Nindza[ idNindze=" + idNindze + " ]";
    }

}
