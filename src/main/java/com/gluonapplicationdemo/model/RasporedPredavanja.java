/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "rasporedPredavanja")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RasporedPredavanja.findAll", query = "SELECT r FROM RasporedPredavanja r")
    , @NamedQuery(name = "RasporedPredavanja.findByIdRasPred", query = "SELECT r FROM RasporedPredavanja r WHERE r.idRasPred = :idRasPred")
    , @NamedQuery(name = "RasporedPredavanja.findByNazivPredavanja", query = "SELECT r FROM RasporedPredavanja r WHERE r.nazivPredavanja = :nazivPredavanja")
    , @NamedQuery(name = "RasporedPredavanja.findByKurs", query = "SELECT r FROM RasporedPredavanja r WHERE r.kurs = :kurs")
    , @NamedQuery(name = "RasporedPredavanja.findByKlan", query = "SELECT r FROM RasporedPredavanja r WHERE r.klan = :klan")
    , @NamedQuery(name = "RasporedPredavanja.findByPredavac", query = "SELECT r FROM RasporedPredavanja r WHERE r.predavac = :predavac")
    , @NamedQuery(name = "RasporedPredavanja.findByMaterijal", query = "SELECT r FROM RasporedPredavanja r WHERE r.materijal = :materijal")
    , @NamedQuery(name = "RasporedPredavanja.findByPocetakPred", query = "SELECT r FROM RasporedPredavanja r WHERE r.pocetakPred = :pocetakPred")
    , @NamedQuery(name = "RasporedPredavanja.findByKrajPredavanja", query = "SELECT r FROM RasporedPredavanja r WHERE r.krajPredavanja = :krajPredavanja")})
public class RasporedPredavanja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRasPred")
    private Integer idRasPred;
    @Basic(optional = false)
    @Column(name = "nazivPredavanja")
    private String nazivPredavanja;
    @Basic(optional = false)
    @Column(name = "kurs")
    private int kurs;
    @Basic(optional = false)
    @Column(name = "klan")
    private int klan;
    @Basic(optional = false)
    @Column(name = "predavac")
    private int predavac;
    @Column(name = "materijal")
    private Integer materijal;
    @Basic(optional = false)
    @Column(name = "pocetakPred")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pocetakPred;
    @Basic(optional = false)
    @Column(name = "krajPredavanja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date krajPredavanja;

    public RasporedPredavanja() {
    }

    public RasporedPredavanja(Integer idRasPred) {
        this.idRasPred = idRasPred;
    }

    public RasporedPredavanja(Integer idRasPred, String nazivPredavanja, int kurs, int klan, int predavac, Date pocetakPred, Date krajPredavanja) {
        this.idRasPred = idRasPred;
        this.nazivPredavanja = nazivPredavanja;
        this.kurs = kurs;
        this.klan = klan;
        this.predavac = predavac;
        this.pocetakPred = pocetakPred;
        this.krajPredavanja = krajPredavanja;
    }

    public Integer getIdRasPred() {
        return idRasPred;
    }

    public void setIdRasPred(Integer idRasPred) {
        this.idRasPred = idRasPred;
    }

    public String getNazivPredavanja() {
        return nazivPredavanja;
    }

    public void setNazivPredavanja(String nazivPredavanja) {
        this.nazivPredavanja = nazivPredavanja;
    }

    public int getKurs() {
        return kurs;
    }

    public void setKurs(int kurs) {
        this.kurs = kurs;
    }

    public int getKlan() {
        return klan;
    }

    public void setKlan(int klan) {
        this.klan = klan;
    }

    public int getPredavac() {
        return predavac;
    }

    public void setPredavac(int predavac) {
        this.predavac = predavac;
    }

    public Integer getMaterijal() {
        return materijal;
    }

    public void setMaterijal(Integer materijal) {
        this.materijal = materijal;
    }

    public Date getPocetakPred() {
        return pocetakPred;
    }

    public void setPocetakPred(Date pocetakPred) {
        this.pocetakPred = pocetakPred;
    }

    public Date getKrajPredavanja() {
        return krajPredavanja;
    }

    public void setKrajPredavanja(Date krajPredavanja) {
        this.krajPredavanja = krajPredavanja;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRasPred != null ? idRasPred.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RasporedPredavanja)) {
            return false;
        }
        RasporedPredavanja other = (RasporedPredavanja) object;
        if ((this.idRasPred == null && other.idRasPred != null) || (this.idRasPred != null && !this.idRasPred.equals(other.idRasPred))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.RasporedPredavanja[ idRasPred=" + idRasPred + " ]";
    }

}
