/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "nivoPristupa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NivoPristupa.findAll", query = "SELECT n FROM NivoPristupa n")
    , @NamedQuery(name = "NivoPristupa.findByIdnivoPristupa", query = "SELECT n FROM NivoPristupa n WHERE n.idnivoPristupa = :idnivoPristupa")
    , @NamedQuery(name = "NivoPristupa.findByNivoPristupa", query = "SELECT n FROM NivoPristupa n WHERE n.nivoPristupa = :nivoPristupa")})
public class NivoPristupa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_nivoPristupa")
    private Integer idnivoPristupa;
    @Basic(optional = false)
    @Column(name = "nivoPristupa")
    private String nivoPristupa;

    public NivoPristupa() {
    }

    public NivoPristupa(Integer idnivoPristupa) {
        this.idnivoPristupa = idnivoPristupa;
    }

    public NivoPristupa(Integer idnivoPristupa, String nivoPristupa) {
        this.idnivoPristupa = idnivoPristupa;
        this.nivoPristupa = nivoPristupa;
    }

    public Integer getIdnivoPristupa() {
        return idnivoPristupa;
    }

    public void setIdnivoPristupa(Integer idnivoPristupa) {
        this.idnivoPristupa = idnivoPristupa;
    }

    public String getNivoPristupa() {
        return nivoPristupa;
    }

    public void setNivoPristupa(String nivoPristupa) {
        this.nivoPristupa = nivoPristupa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idnivoPristupa != null ? idnivoPristupa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NivoPristupa)) {
            return false;
        }
        NivoPristupa other = (NivoPristupa) object;
        if ((this.idnivoPristupa == null && other.idnivoPristupa != null) || (this.idnivoPristupa != null && !this.idnivoPristupa.equals(other.idnivoPristupa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.NivoPristupa[ idnivoPristupa=" + idnivoPristupa + " ]";
    }

}
