/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "oglasnaTabla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OglasnaTabla.findAll", query = "SELECT o FROM OglasnaTabla o")
    , @NamedQuery(name = "OglasnaTabla.findByIdoglasnaTabla", query = "SELECT o FROM OglasnaTabla o WHERE o.idoglasnaTabla = :idoglasnaTabla")
    , @NamedQuery(name = "OglasnaTabla.findByIdNindze", query = "SELECT o FROM OglasnaTabla o WHERE o.idNindze = :idNindze")
    , @NamedQuery(name = "OglasnaTabla.findByTekstPoruke", query = "SELECT o FROM OglasnaTabla o WHERE o.tekstPoruke = :tekstPoruke")
    , @NamedQuery(name = "OglasnaTabla.findByVremePoruke", query = "SELECT o FROM OglasnaTabla o WHERE o.vremePoruke = :vremePoruke")})
public class OglasnaTabla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_oglasnaTabla")
    private Integer idoglasnaTabla;
    @Basic(optional = false)
    @Column(name = "idNindze")
    private int idNindze;
    @Basic(optional = false)
    @Column(name = "tekstPoruke")
    private String tekstPoruke;
    @Basic(optional = false)
    @Column(name = "vremePoruke")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vremePoruke;

    public OglasnaTabla() {
    }

    public OglasnaTabla(Integer idoglasnaTabla) {
        this.idoglasnaTabla = idoglasnaTabla;
    }

    public OglasnaTabla(Integer idoglasnaTabla, int idNindze, String tekstPoruke, Date vremePoruke) {
        this.idoglasnaTabla = idoglasnaTabla;
        this.idNindze = idNindze;
        this.tekstPoruke = tekstPoruke;
        this.vremePoruke = vremePoruke;
    }

    public Integer getIdoglasnaTabla() {
        return idoglasnaTabla;
    }

    public void setIdoglasnaTabla(Integer idoglasnaTabla) {
        this.idoglasnaTabla = idoglasnaTabla;
    }

    public int getIdNindze() {
        return idNindze;
    }

    public void setIdNindze(int idNindze) {
        this.idNindze = idNindze;
    }

    public String getTekstPoruke() {
        return tekstPoruke;
    }

    public void setTekstPoruke(String tekstPoruke) {
        this.tekstPoruke = tekstPoruke;
    }

    public Date getVremePoruke() {
        return vremePoruke;
    }

    public void setVremePoruke(Date vremePoruke) {
        this.vremePoruke = vremePoruke;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idoglasnaTabla != null ? idoglasnaTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OglasnaTabla)) {
            return false;
        }
        OglasnaTabla other = (OglasnaTabla) object;
        if ((this.idoglasnaTabla == null && other.idoglasnaTabla != null) || (this.idoglasnaTabla != null && !this.idoglasnaTabla.equals(other.idoglasnaTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.OglasnaTabla[ idoglasnaTabla=" + idoglasnaTabla + " ]";
    }

}
