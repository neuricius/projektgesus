/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "kursevi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kursevi.findAll", query = "SELECT k FROM Kursevi k")
    , @NamedQuery(name = "Kursevi.findByIdKursevi", query = "SELECT k FROM Kursevi k WHERE k.idKursevi = :idKursevi")
    , @NamedQuery(name = "Kursevi.findByNazivKursa", query = "SELECT k FROM Kursevi k WHERE k.nazivKursa = :nazivKursa")})
public class Kursevi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idKursevi")
    private Integer idKursevi;
    @Basic(optional = false)
    @Column(name = "nazivKursa")
    private String nazivKursa;

    public Kursevi() {
    }

    public Kursevi(Integer idKursevi) {
        this.idKursevi = idKursevi;
    }

    public Kursevi(Integer idKursevi, String nazivKursa) {
        this.idKursevi = idKursevi;
        this.nazivKursa = nazivKursa;
    }

    public Integer getIdKursevi() {
        return idKursevi;
    }

    public void setIdKursevi(Integer idKursevi) {
        this.idKursevi = idKursevi;
    }

    public String getNazivKursa() {
        return nazivKursa;
    }

    public void setNazivKursa(String nazivKursa) {
        this.nazivKursa = nazivKursa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKursevi != null ? idKursevi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kursevi)) {
            return false;
        }
        Kursevi other = (Kursevi) object;
        if ((this.idKursevi == null && other.idKursevi != null) || (this.idKursevi != null && !this.idKursevi.equals(other.idKursevi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Kursevi[ idKursevi=" + idKursevi + " ]";
    }

}
