/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "materijali")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materijali.findAll", query = "SELECT m FROM Materijali m")
    , @NamedQuery(name = "Materijali.findByIdMaterijali", query = "SELECT m FROM Materijali m WHERE m.idMaterijali = :idMaterijali")
    , @NamedQuery(name = "Materijali.findByNazivMaterijala", query = "SELECT m FROM Materijali m WHERE m.nazivMaterijala = :nazivMaterijala")})
public class Materijali implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMaterijali")
    private Integer idMaterijali;
    @Basic(optional = false)
    @Column(name = "nazivMaterijala")
    private String nazivMaterijala;
    @Lob
    @Column(name = "fajlMaterijala")
    private byte[] fajlMaterijala;

    public Materijali() {
    }

    public Materijali(Integer idMaterijali) {
        this.idMaterijali = idMaterijali;
    }

    public Materijali(Integer idMaterijali, String nazivMaterijala) {
        this.idMaterijali = idMaterijali;
        this.nazivMaterijala = nazivMaterijala;
    }

    public Integer getIdMaterijali() {
        return idMaterijali;
    }

    public void setIdMaterijali(Integer idMaterijali) {
        this.idMaterijali = idMaterijali;
    }

    public String getNazivMaterijala() {
        return nazivMaterijala;
    }

    public void setNazivMaterijala(String nazivMaterijala) {
        this.nazivMaterijala = nazivMaterijala;
    }

    public byte[] getFajlMaterijala() {
        return fajlMaterijala;
    }

    public void setFajlMaterijala(byte[] fajlMaterijala) {
        this.fajlMaterijala = fajlMaterijala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaterijali != null ? idMaterijali.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materijali)) {
            return false;
        }
        Materijali other = (Materijali) object;
        if ((this.idMaterijali == null && other.idMaterijali != null) || (this.idMaterijali != null && !this.idMaterijali.equals(other.idMaterijali))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Materijali[ idMaterijali=" + idMaterijali + " ]";
    }

}
