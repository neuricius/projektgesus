/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gluonapplicationdemo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nebojsa Matic
 */
@Entity
@Table(name = "klanovi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Klanovi.findAll", query = "SELECT k FROM Klanovi k")
    , @NamedQuery(name = "Klanovi.findByIdKlana", query = "SELECT k FROM Klanovi k WHERE k.idKlana = :idKlana")
    , @NamedQuery(name = "Klanovi.findByNazivKlana", query = "SELECT k FROM Klanovi k WHERE k.nazivKlana = :nazivKlana")})
public class Klanovi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idKlana")
    private Integer idKlana;
    @Basic(optional = false)
    @Column(name = "nazivKlana")
    private String nazivKlana;

    public Klanovi() {
    }

    public Klanovi(Integer idKlana) {
        this.idKlana = idKlana;
    }

    public Klanovi(Integer idKlana, String nazivKlana) {
        this.idKlana = idKlana;
        this.nazivKlana = nazivKlana;
    }

    public Integer getIdKlana() {
        return idKlana;
    }

    public void setIdKlana(Integer idKlana) {
        this.idKlana = idKlana;
    }

    public String getNazivKlana() {
        return nazivKlana;
    }

    public void setNazivKlana(String nazivKlana) {
        this.nazivKlana = nazivKlana;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKlana != null ? idKlana.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Klanovi)) {
            return false;
        }
        Klanovi other = (Klanovi) object;
        if ((this.idKlana == null && other.idKlana != null) || (this.idKlana != null && !this.idKlana.equals(other.idKlana))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Klanovi[ idKlana=" + idKlana + " ]";
    }

}
