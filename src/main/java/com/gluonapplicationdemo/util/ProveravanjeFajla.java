/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.util;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupa2
 */
public class ProveravanjeFajla {

    public void vratiTekstIzFajla() {
        try {
            ArrayList<String> sadrzajFajla = new ArrayList<>();
            InputStream inpstr = ProveravanjeFajla.class.getResourceAsStream("idhash.txt");
            InputStreamReader streamReader = new InputStreamReader(inpstr, StandardCharsets.UTF_8);
            BufferedReader Breader = new BufferedReader(streamReader);
            if (!Breader.readLine().isEmpty()) {
                String line = Breader.readLine();
                while (line != null) { 
                    sadrzajFajla.add(line);
                }
                Breader.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(ProveravanjeFajla.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
