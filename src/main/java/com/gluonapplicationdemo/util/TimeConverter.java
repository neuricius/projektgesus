/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Nebojsa Matic
 */
public class TimeConverter {

    public static String vratiDatum(Date datum) {
        String pattern = "dd-MM-yy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(datum);

        return date;
    }

    public static String vratiVreme(Date datum) {
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String time = simpleDateFormat.format(datum);

        return time;
    }

    public static String izracunajTrajanje(Date stariDatum, Date noviDatum, TimeUnit jedinicaVremena) {
        long diff = noviDatum.getTime() - stariDatum.getTime();
        return String.valueOf(jedinicaVremena.convert(diff, TimeUnit.MILLISECONDS));
    }
}
