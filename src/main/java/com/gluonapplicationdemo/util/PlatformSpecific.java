/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.util;

import com.gluonhq.charm.down.Platform;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

/**
 *
 * @author Nebojsa Matic
 */
public class PlatformSpecific {

    public static Rectangle2D dimenzijeEkrana() {
        if (Platform.isDesktop()) {
            return Screen.getPrimary().getBounds();
        } else {
            return Screen.getPrimary().getVisualBounds();
        }
    }
}
