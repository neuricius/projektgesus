package com.gluonapplicationdemo.util;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class CryptoMaster {

    public static byte[] encrypt(String poruka, String kljuc) {
        byte[] encrypted = null;
        try {
            String text = poruka;
            String key = generisanjeKljuca(kljuc); // 128 bit key
            // Create key and cipher
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            encrypted = cipher.doFinal(text.getBytes());
            // decrypt the text
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return encrypted;
        }
    }

    public static String decrypt(byte[] poruka, String kljuc) {
        String decrstr = "";
        try {
            String key = generisanjeKljuca(kljuc); // 128 bit key
            // Create key and cipher
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            // decrypt the text
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            decrstr = new String(cipher.doFinal(poruka));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return decrstr;
        }
    }

    public static String generisanjeKljuca(String str) {
        String finalStr = "";
        String s = str;
        s = s.replace(" ", "");
        s = s.replaceAll("[\\\\@./:*?\"<>|]", "");
        char[] c = s.toCharArray();

        for (Character ss : c) {
            finalStr += (ss - 'a' + 1);
            finalStr += ss;
            finalStr += (ss + 'a' + 1);
        }
        return finalStr.substring(4, 20);
    }

    public static String generisanjeHasha(String str) {
        String finalStr = "";
        String s = str;
        s = s.replace(" ", "");
        s = s.replaceAll("[\\\\@./:*?\"<>|]", "");
        char[] c = s.toCharArray();

        for (Character ss : c) {
            finalStr += (ss - 'a' + 1);
            finalStr += ss;
            finalStr += (ss + 'a' + 1);
        }
        return finalStr;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /*                                    *
    prebacuje niz bajtova u hex string 
   *                                    */
    public static String byteArrayToHexString(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xff;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0f];
        }
        return new String(hexChars);
    }
}
