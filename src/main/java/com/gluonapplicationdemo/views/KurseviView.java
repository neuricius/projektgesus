/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.views;

import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.PicturesService;
import com.gluonhq.charm.down.plugins.ShareService;
import com.gluonhq.charm.down.plugins.StorageService;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author Grupa2
 */
public class KurseviView extends View {

    public KurseviView() {

        Label label = new Label("Lista Kurseva");

        VBox controls = new VBox();
        controls.getChildren().add(label);
        controls.setAlignment(Pos.CENTER);

        setCenter(controls);

        setShowTransitionFactory(BounceInRightTransition::new);

        FloatingActionButton floatingActionButton = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                (ActionEvent e) -> {
                    ImageView imageView = new ImageView();
                    Services.get(PicturesService.class).ifPresent(service -> {
                        service.takePhoto(false).ifPresent(image -> imageView.setImage(image));
                    });
                    controls.getChildren().clear();
                    controls.getChildren().add(imageView);
                    //                    buttonFile.setOnAction(e2 -> s.share("Sharing an icon", "Hi, I send you an image.", "image/png", file));

//                    Button buttonFile = new Button("Share Image", MaterialDesignIcon.SHARE.graphic());
//                    Services.get(SharedService.class).ifPresent(s
//                            -> buttonFile.setOnAction(e2 -> s.share("Sharing an icon", "Hi, I send you an image.", "image/png", file)));
//                    String ime = "Nebojsa";
//                    String prezime = "Matic";
//                    String email = "neske.kv@gmail.com";
//                    String telefon = "39235987325982";
//                    String idstud = "16/2011";
//
////                dodajProfil(ime, prezime, email, telefon, idstud);
//                    System.out.println("Uspesno kreiran profil: " + ime + " " + prezime);
                });
        floatingActionButton.showOn(this);

    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> getApplication().getDrawer().open()));
        appBar.setTitleText("Kursevi");
        appBar.getActionItems().add(MaterialDesignIcon.FAVORITE.button(e -> System.out.println("Favorite")));
    }

    private static void copyFromResources(String pathIni, String pathEnd, String name) {

        try (InputStream myInput = KurseviView.class.getResourceAsStream(pathIni + name)) {
            String outFileName = pathEnd + "/" + name;
            try (OutputStream myOutput = new FileOutputStream(outFileName)) {
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }
                myOutput.flush();

            } catch (IOException ex) {
                System.out.println("Error " + ex);
            }
        } catch (IOException ex) {
            System.out.println("Error " + ex);
        }
    }
}
