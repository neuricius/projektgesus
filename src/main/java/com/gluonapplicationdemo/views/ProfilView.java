package com.gluonapplicationdemo.views;

import com.gluonapplicationdemo.DrawerManager;
import static com.gluonapplicationdemo.DrawerManager.drawer;
import com.gluonapplicationdemo.model.Nindza;
import static com.gluonapplicationdemo.sql.sqltools.kornjaca;
import static com.gluonapplicationdemo.sql.sqltools.vratiNazivKlanaPoId;
import static com.gluonapplicationdemo.sql.sqltools.vratiNindzuPoImenu;
import static com.gluonapplicationdemo.views.LoginView.imeNindze;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class ProfilView extends View {

    public static Label imeStudentaL, razredStudenta, levelStudenta, nazivKursa1, nazivKursa2;

    public ProfilView() {
        setShowTransitionFactory(BounceInRightTransition::new);

        imeStudentaL = new Label("Stipe");
        imeStudentaL.setFont(Font.font("System", 18));

        razredStudenta = new Label("");

        levelStudenta = new Label("5");

        ProgressBar xpStudenta = new ProgressBar(0.66);

        HBox levelHBox = new HBox(5, levelStudenta, xpStudenta);

        VBox studentInfoVBox = new VBox(5, imeStudentaL, razredStudenta, levelHBox);
        studentInfoVBox.setAlignment(Pos.CENTER_LEFT);
        studentInfoVBox.setPadding(new Insets(0, 0, 0, 5));

        ImageView slikaKorisnika = new ImageView(new Image(ProfilView.class.getResourceAsStream("/profile-placeholder.png"), 100, 100, true, true));

        HBox topInfoHBox = new HBox(5, slikaKorisnika, studentInfoVBox);
        topInfoHBox.setPadding(new Insets(10));

        setTop(topInfoHBox);

        GridPane kurseviGrid = new GridPane();
        kurseviGrid.setGridLinesVisible(true);
        kurseviGrid.setPrefSize(USE_PREF_SIZE, USE_PREF_SIZE);

        ColumnConstraints kolona1 = new ColumnConstraints();
        kolona1.setPercentWidth(40);
        ColumnConstraints kolona2 = new ColumnConstraints();
        kolona2.setPercentWidth(160);
        kurseviGrid.getColumnConstraints().addAll(kolona1, kolona2);

        RowConstraints redovi = new RowConstraints(50);
        kurseviGrid.getRowConstraints().add(redovi);

        //test kod
        ImageView ikonaKursa = new ImageView(new Image(ProfilView.class.getResourceAsStream("/placeholder.png"), 50, 50, true, true));
        ImageView ikonaKursa2 = new ImageView(new Image(ProfilView.class.getResourceAsStream("/placeholder.png"), 50, 50, true, true));

        nazivKursa1 = new Label("Core Java Programming");
        nazivKursa1.setPadding(new Insets(0, 0, 0, 10));
        nazivKursa2 = new Label("Dummy Test Course");
        nazivKursa2.setPadding(new Insets(0, 0, 0, 10));

        kurseviGrid.add(ikonaKursa, 0, 0);
        kurseviGrid.add(ikonaKursa2, 0, 1);
        kurseviGrid.add(nazivKursa1, 1, 0);
        kurseviGrid.add(nazivKursa2, 1, 1);

        ScrollPane kurseviSP = new ScrollPane(kurseviGrid);

        Tab kurseviTab = new Tab("Kursevi");
        kurseviTab.setContent(kurseviSP);
        kurseviTab.setClosable(false);

        Tab achivTab = new Tab("Dostignuca");

        Tab rangListaTab = new Tab("Rang studenta");

        TabPane centralniDeoTP = new TabPane();
        centralniDeoTP.getTabs().add(kurseviTab);

        VBox controls = new VBox();
        controls.setAlignment(Pos.TOP_CENTER);
        controls.setPadding(new Insets(5));
        VBox.setMargin(controls, new Insets(5));

        setCenter(centralniDeoTP);

        setShowTransitionFactory(BounceInRightTransition::new);

        FloatingActionButton floatingActionButton = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("kurcina"));
        floatingActionButton.showOn(this);
        ucitajGlavonju(kornjaca);

    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> getApplication().getDrawer().open()));
        appBar.setTitleText("Profil");
        appBar.getActionItems().add(MaterialDesignIcon.FAVORITE.button(e -> System.out.println("Favorite")));
    }

    public static void ucitajGlavic(Nindza nindza) {
        drawer.setHeader(null);
        String imeStudentaString = nindza.getPrezime() + " " + nindza.getIme();
        String razredStudentaString = vratiNazivKlanaPoId(nindza.getKlanNindze());
        Avatar glavic = new Avatar(21, new Image(DrawerManager.class.getResourceAsStream("/profile-placeholder.png")));
        drawer.setHeader(new NavigationDrawer.Header(imeStudentaString, razredStudentaString, glavic));
    }

    public static void ucitajGlavonju(Nindza nindza) {
        imeStudentaL.setText(nindza.getPrezime() + " " + nindza.getIme());
        razredStudenta.setText(vratiNazivKlanaPoId(nindza.getKlanNindze()));
    }
}
