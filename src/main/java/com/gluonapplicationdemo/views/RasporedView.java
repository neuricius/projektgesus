/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.views;

import com.gluonapplicationdemo.model.Nindza;
import com.gluonapplicationdemo.model.RasporedPredavanja;
import static com.gluonapplicationdemo.sql.sqltools.kornjaca;
import static com.gluonapplicationdemo.sql.sqltools.napraviListuKlanova;
import static com.gluonapplicationdemo.sql.sqltools.napraviListuKurseva;
import static com.gluonapplicationdemo.sql.sqltools.napraviRasporedPredavanja;
import static com.gluonapplicationdemo.sql.sqltools.vratiIdIzMape;
import static com.gluonapplicationdemo.sql.sqltools.vratiNindzuPoId;
import static com.gluonapplicationdemo.util.TimeConverter.izracunajTrajanje;
import static com.gluonapplicationdemo.util.TimeConverter.vratiVreme;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Grupa2
 */
public class RasporedView extends View {

    VBox predavanjaVBox;

    public RasporedView() {
        VBox controls = new VBox();
        controls.setAlignment(Pos.TOP_CENTER);
        controls.setSpacing(5);
        setCenter(controls);

        Label nazivL = new Label("Raspored predavanja");
        nazivL.setAlignment(Pos.CENTER);
        controls.getChildren().add(nazivL);

        ComboBox<String> listaKlanova = new ComboBox<>();
        napraviListuKlanova().forEach((klan) -> {
            listaKlanova.getItems().add(klan.getNazivKlana());
        });
        listaKlanova.setPromptText("Odeljenje");

        ComboBox<String> listaKurseva = new ComboBox<>();
        napraviListuKurseva().forEach((kurs) -> {
            listaKurseva.getItems().add(kurs.getNazivKursa());
        });
        listaKurseva.setPromptText("Kurs");

        HBox izborHBox = new HBox(5, listaKlanova, listaKurseva);
        controls.getChildren().add(izborHBox);

        predavanjaVBox = new VBox(5);
        predavanjaVBox.setAlignment(Pos.CENTER);
        predavanjaVBox.setPadding(new Insets(5));
        controls.getChildren().add(predavanjaVBox);

        listaKlanova.setOnAction(e -> {
            predavanjaVBox.getChildren().clear();
            iscrtajPredavanja(predavanjaVBox, listaKlanova.getValue(), listaKurseva.getValue());
        });
        listaKurseva.setOnAction(e -> {
            predavanjaVBox.getChildren().clear();
            iscrtajPredavanja(predavanjaVBox, listaKlanova.getValue(), listaKurseva.getValue());
        });

        setShowTransitionFactory(BounceInRightTransition::new);
        FloatingActionButton fabDodajCas = new FloatingActionButton(MaterialDesignIcon.ADD.text,
                e -> System.out.println("Info"));
        if (kornjaca.getNivo() == 2) {
            fabDodajCas.show();
        } else {
            fabDodajCas.hide();
        }
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> getApplication().getDrawer().open()));
        appBar.setTitleText("Raspored");
        if (kornjaca.getNivo() == 2) {
            appBar.getActionItems().add(MaterialDesignIcon.ADD.button(e -> System.out.println("Favorite")));
        }
    }

    public static void iscrtajPredavanja(VBox vbox, String klan, String kurs) {
        ArrayList<RasporedPredavanja> raspored = napraviRasporedPredavanja();
        int idKlana, idKursa;
        if (klan == null) {
            idKlana = 0;
        } else {
            idKlana = vratiIdIzMape("klanovi", klan);
        }
        if (kurs == null) {
            idKursa = 0;
        } else {
            idKursa = vratiIdIzMape("kursevi", kurs);
        }

        for (RasporedPredavanja predavanje : raspored) {
            if ((predavanje.getKlan() == idKlana && predavanje.getKurs() == idKursa)
                    || (idKlana == 0 && predavanje.getKurs() == idKursa)
                    || (predavanje.getKlan() == idKlana && idKursa == 0)) {
                Label pocetakPredavanja = new Label();
                pocetakPredavanja.setText(vratiVreme(predavanje.getPocetakPred()));
                pocetakPredavanja.setTextAlignment(TextAlignment.LEFT);

                Label trajanjePredavanja = new Label();
                trajanjePredavanja.setText(izracunajTrajanje(predavanje.getPocetakPred(), predavanje.getKrajPredavanja(), TimeUnit.MINUTES) + "m");

                VBox infoVremeVBox = new VBox(5, pocetakPredavanja, trajanjePredavanja);
                infoVremeVBox.setAlignment(Pos.CENTER);
                infoVremeVBox.setPadding(new Insets(5));

                Label nazivPredavanja = new Label();
                nazivPredavanja.setText(predavanje.getNazivPredavanja());
                nazivPredavanja.setPrefWidth(150);
                nazivPredavanja.setTextAlignment(TextAlignment.CENTER);
                nazivPredavanja.setAlignment(Pos.CENTER);
                nazivPredavanja.setWrapText(true);

                Label imePredavaca = new Label();
                Nindza splinter = vratiNindzuPoId(predavanje.getPredavac());
                String imePrezimePredavaca = splinter.getPrezime() + " " + splinter.getIme();
                imePredavaca.setText(imePrezimePredavaca);

                Label materijalZaPred = new Label();
                materijalZaPred.setText("materijal");
                materijalZaPred.setTextAlignment(TextAlignment.CENTER);
                materijalZaPred.setAlignment(Pos.CENTER);

                VBox dodatniInfoVBox = new VBox(5, imePredavaca, materijalZaPred);
                dodatniInfoVBox.setAlignment(Pos.CENTER_RIGHT);

                HBox predavanjeGlavniHBox = new HBox(5, infoVremeVBox, nazivPredavanja, dodatniInfoVBox);
                predavanjeGlavniHBox.setStyle("-fx-border-color: grey;");
                predavanjeGlavniHBox.setAlignment(Pos.CENTER);

                vbox.getChildren().add(0, predavanjeGlavniHBox);
            } else {
                Label nemaPredavanja = new Label("Nema predavanja za Vas kurs/razred");
                nemaPredavanja.setAlignment(Pos.CENTER);
                nemaPredavanja.setTextAlignment(TextAlignment.CENTER);

                HBox predavanjeGlavniHBox = new HBox(5, nemaPredavanja);
                predavanjeGlavniHBox.setAlignment(Pos.CENTER);

                vbox.getChildren().add(0, predavanjeGlavniHBox);
            }
        }
    }

}
