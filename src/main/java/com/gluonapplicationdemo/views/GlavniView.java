/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.views;

import com.gluonapplicationdemo.model.OglasnaTabla;
import static com.gluonapplicationdemo.sql.sqltools.dodajPoruku;
import static com.gluonapplicationdemo.sql.sqltools.kornjaca;
import static com.gluonapplicationdemo.sql.sqltools.skupiPorukeZaTablu;
import static com.gluonapplicationdemo.sql.sqltools.vratiNindzuPoId;
import static com.gluonapplicationdemo.util.TimeConverter.vratiDatum;
import static com.gluonapplicationdemo.util.TimeConverter.vratiVreme;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.control.TextField;
import com.gluonhq.charm.glisten.control.Toast;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

/**
 *
 * @author Grupa2
 */
public class GlavniView extends View {
    
    VBox controls;

    public GlavniView() {

        controls = new VBox();
        controls.setPadding(new Insets(5));
        controls.setMinSize(100, 100);
        controls.setSpacing(10);


        ispisiPorukeNaTabli(controls, skupiPorukeZaTablu());

        ScrollPane controlsSP = new ScrollPane(controls);

        setCenter(controlsSP);

        getStylesheets().add(PrimaryView.class.getResource("primary.css").toExternalForm());
        setShowTransitionFactory(BounceInRightTransition::new);
        FloatingActionButton nekoDugme = new FloatingActionButton(MaterialDesignIcon.REFRESH.text,
                e -> osveziPoruke());
        nekoDugme.showOn(this);
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> getApplication().getDrawer().open()));
        appBar.setTitleText("Glavna");
        appBar.getActionItems().add(MaterialDesignIcon.ADD.button(e -> dodajNovuPoruku()));
    }

    private void ispisiPorukeNaTabli(VBox vbox, ArrayList<OglasnaTabla> lista) {
        if (lista.isEmpty()) {
            Label nemaPoruka = new Label("Nema poruka na tabli");
            nemaPoruka.setAlignment(Pos.CENTER);
            nemaPoruka.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(0), new Insets(0, 0, 0, 0))));
            nemaPoruka.setStyle("-fx-border-color: grey;");
            vbox.getChildren().add(nemaPoruka);
        } else {
            for (OglasnaTabla poruka : lista) {
                Label tekstPoruke = new Label(poruka.getTekstPoruke());
                tekstPoruke.setWrapText(true);
                tekstPoruke.setTextAlignment(TextAlignment.JUSTIFY);
                tekstPoruke.setPrefWidth(300);

                Label autorPoruke = new Label(vratiNindzuPoId(poruka.getIdNindze()).getIme());
                autorPoruke.setTextAlignment(TextAlignment.LEFT);

                Label vremePoruke = new Label();
                vremePoruke.setText(vratiVreme(poruka.getVremePoruke()));
                vremePoruke.setTextAlignment(TextAlignment.RIGHT);

                Label datumPoruke = new Label();
                datumPoruke.setText(vratiDatum(poruka.getVremePoruke()));
                datumPoruke.setTextAlignment(TextAlignment.RIGHT);

                HBox infoOPoruciHBox = new HBox(5, autorPoruke, datumPoruke, vremePoruke);
                infoOPoruciHBox.setAlignment(Pos.BASELINE_LEFT);

                VBox celaPorukaVBox = new VBox(5);
                celaPorukaVBox.setAlignment(Pos.CENTER);
                celaPorukaVBox.setBackground(new Background(new BackgroundFill(Color.gray(0.92), new CornerRadii(0), new Insets(0, 0, 0, 0))));
                celaPorukaVBox.setStyle("-fx-border-color: grey;");
                celaPorukaVBox.getChildren().addAll(tekstPoruke, infoOPoruciHBox);
                AnchorPane.setLeftAnchor(celaPorukaVBox, 0.0);
                AnchorPane.setRightAnchor(celaPorukaVBox, 0.0);
                vbox.getChildren().add(0, celaPorukaVBox);
            }
        }
    }

    private void osveziPoruke() {
        controls.getChildren().clear();
        ispisiPorukeNaTabli(controls, skupiPorukeZaTablu());
    }

    private void dodajNovuPoruku() {
        Dialog novaPoruka = new Dialog();
        novaPoruka.setTitleText("Nova Poruka");

        Label tekstPorukeL = new Label("Tekst poruke");
        tekstPorukeL.setAlignment(Pos.CENTER);
        TextField tekstTF = new TextField();
        tekstTF.setMinWidth(50);

        VBox tekstPorukeVBox = new VBox(5, tekstPorukeL, tekstTF);
        tekstPorukeVBox.setAlignment(Pos.TOP_LEFT);

        novaPoruka.setContent(tekstPorukeVBox);

        Button okisha = new Button("Dodaj");
        okisha.setOnAction(e -> {
            String tekst = tekstTF.getText();
            int id = kornjaca.getIdNindze();
                dodajPoruku(id, tekst);
                osveziPoruke();
                novaPoruka.hide();
                Toast toster = new Toast("Poruka uspesno dodata", Duration.seconds(3));
        });
        
        Button otkisha = new Button("Otkazi");
        otkisha.setOnAction(e -> {
            Dialog potvrdaOtkaza = new Dialog("Otkaz", "Da li ste sigurni?");
            Button okiB = new Button("Da");
            okiB.setOnAction(e2 -> {
                potvrdaOtkaza.hide();
                novaPoruka.hide();
            });
            Button neB = new Button("Ne");
            neB.setOnAction(e3 -> {
                e3.consume();
                e.consume();
                potvrdaOtkaza.hide();
            });
            potvrdaOtkaza.getButtons().addAll(okiB, neB);
            potvrdaOtkaza.showAndWait();
        });
        
        novaPoruka.getButtons().addAll(okisha, otkisha);
        novaPoruka.showAndWait();
    }
}
