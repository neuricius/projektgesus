/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gluonapplicationdemo.views;

import static com.gluonapplicationdemo.GluonApplicationDemo.SECOND_VIEW;
import static com.gluonapplicationdemo.sql.sqltools.dodajNindzu;
import static com.gluonapplicationdemo.sql.sqltools.nindzaExists;
import static com.gluonapplicationdemo.sql.sqltools.vratiSifruIzBaze;
import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.application.ViewStackPolicy;
import com.gluonhq.charm.glisten.control.Dialog;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.control.TextField;
import com.gluonhq.charm.glisten.control.Toast;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import static com.gluonapplicationdemo.sql.sqltools.napraviListuKlanova;
import static com.gluonapplicationdemo.sql.sqltools.vratiNindzuPoImenu;
import static com.gluonapplicationdemo.util.CryptoMaster.byteArrayToHexString;
import static com.gluonapplicationdemo.util.CryptoMaster.decrypt;
import static com.gluonapplicationdemo.util.CryptoMaster.encrypt;
import static com.gluonapplicationdemo.util.CryptoMaster.hexStringToByteArray;
import static com.gluonapplicationdemo.views.ProfilView.ucitajGlavic;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 *
 * @author Grupa2
 */
public class LoginView extends View {

    private static TextField emailTF;
    private static PasswordField passwordTF;

    public static String imeNindze;
    public static ArrayList<String> templista;

    public LoginView() {

        Label korisnickoImeL = new Label("Email");
        Label sifra = new Label("Sifra");
        VBox labeleVbox = new VBox(5, korisnickoImeL, sifra);
        labeleVbox.setAlignment(Pos.CENTER_RIGHT);
        emailTF = new TextField();
        passwordTF = new PasswordField();
        VBox poljaVBox = new VBox(5, emailTF, passwordTF);
        poljaVBox.setAlignment(Pos.CENTER);
        HBox loginHBox = new HBox(5, labeleVbox, poljaVBox);
        loginHBox.setAlignment(Pos.CENTER);
        Button loginB = new Button("Loguj se");
        CheckBox ostaviUlogovanog = new CheckBox("Ostavi me ulogovanog");
        loginB.setOnAction(e -> {
            imeNindze = emailTF.getText();
            if (loginProvera(imeNindze, passwordTF.getText())) {
                //ovde ubaciti kod da ostane ulogovan
                if (ostaviUlogovanog.isSelected()) {
                    upisCookija(imeNindze, passwordTF.getText());
                }
                Dialog pozdrav = new Dialog();
                pozdrav.setContentText("Dobrodosli!");
                Button okisha = new Button("Ok");
                okisha.setOnAction(e2 -> {
                    pozdrav.hide();
                });
                pozdrav.getButtons().add(okisha);
                ucitajGlavic(vratiNindzuPoImenu(imeNindze));
                pozdrav.showAndWait();
                MobileApplication.getInstance().switchView(SECOND_VIEW, ViewStackPolicy.SKIP);
            } else {
                e.consume();
                System.out.println("jbg");
            }
        });
        Button cancelB = new Button("Otkazi");
        cancelB.setOnAction(e -> {
            Dialog pitanje = new Dialog("Prekid logovanja", "Da li ste sigurni?");
            Button okB = new Button("Da");
            okB.setOnAction(e2 -> {
                System.exit(0);
            });
            Button neB = new Button("Ne");
            neB.setOnAction(e3 -> {
                pitanje.hide();
            });
            pitanje.getButtons().addAll(okB, neB);
            pitanje.showAndWait();
        });
        HBox dugmadHBox = new HBox(10, loginB, cancelB);
        dugmadHBox.setAlignment(Pos.CENTER);
        Label regL = new Label("Nemate nalog?");
        regL.setAlignment(Pos.CENTER);
        Button regB = new Button();
        regB.setText("Registrujte se");
        regB.setOnAction(e -> {regujteSe();});
        VBox regVBox = new VBox(10, regL, regB);
        regVBox.setAlignment(Pos.CENTER);
        VBox opcijeVBox = new VBox(5, dugmadHBox, ostaviUlogovanog, regVBox);
        opcijeVBox.setAlignment(Pos.CENTER);
        VBox controls = new VBox(10, loginHBox, opcijeVBox);
        controls.setAlignment(Pos.CENTER);
        setCenter(controls);
        setShowTransitionFactory(BounceInRightTransition::new);

    }

    public void regujteSe() {
        Dialog registracija = new Dialog();
        registracija.setTitle(new Label("Registracija"));
        Label regEmail = new Label("Email: ");
        Label regPassW = new Label("Sifra: ");
        Label imeKor = new Label("Ime: ");
        Label prezimeKor = new Label("Prezime: ");
        Label klanL = new Label("Odeljenje: ");
        VBox regLabeleVBox = new VBox(10, regEmail, regPassW, imeKor, prezimeKor, klanL);
        regLabeleVBox.setAlignment(Pos.TOP_RIGHT);

        TextField regEmailTF = new TextField();
        PasswordField regPassTF = new PasswordField();
        TextField regImeTF = new TextField();
        TextField regPrezimeTF = new TextField();
        ComboBox<String> listaKlanova = new ComboBox<>();
        napraviListuKlanova().forEach((klan) -> {
            listaKlanova.getItems().add(klan.getNazivKlana());
        });

        VBox regPoljaVBox = new VBox(regEmailTF, regPassTF, regImeTF, regPrezimeTF, listaKlanova);
        regPoljaVBox.setAlignment(Pos.TOP_LEFT);

        HBox regHBox = new HBox(regLabeleVBox, regPoljaVBox);
        regHBox.setAlignment(Pos.BASELINE_CENTER);

        registracija.setContent(regHBox);

        Button regOKbutton = new Button("Potvrdi");
        regOKbutton.setOnAction(e -> {
            if (nindzaExists(regEmailTF.getText())) {
                Dialog imePostoji = new Dialog("Nindza vec postoji", "Nindza vec postoji");
                Button okisha = new Button("Ok");
                okisha.setOnAction(e2 -> {
                    e2.consume();
                    imePostoji.hide();
                    regEmailTF.requestFocus();
                });
                imePostoji.getButtons().add(okisha);
                imePostoji.showAndWait();
            } else {
                dodajNindzu(regImeTF.getText(), regPrezimeTF.getText(), regEmailTF.getText(), "", "", regPassTF.getText());
                Toast brvCare = new Toast("Korisnik uspesno dodat", Duration.seconds(3));
                registracija.hide();
                brvCare.show();
            }
        });
        Button regCancelButton = new Button("Otkazi");
        regCancelButton.setOnAction(e -> {
            Dialog potvrdaOtkaza = new Dialog("Otkaz", "Da li ste sigurni?");
            Button okiB = new Button("Da");
            okiB.setOnAction(e2 -> {
                potvrdaOtkaza.hide();
                registracija.hide();
            });
            Button neB = new Button("Ne");
            neB.setOnAction(e3 -> {
                e3.consume();
                e.consume();
                potvrdaOtkaza.hide();
            });
            potvrdaOtkaza.getButtons().addAll(okiB, neB);
            potvrdaOtkaza.showAndWait();
        });
        registracija.getButtons().addAll(regOKbutton, regCancelButton);
        registracija.showAndWait();
    }

    public static boolean proveraCookija() {
        try {
            templista = new ArrayList<>();
            InputStream inpstr = LoginView.class.getResourceAsStream("/idhash.txt");
            InputStreamReader streamReader = new InputStreamReader(inpstr, StandardCharsets.UTF_8);
            try (BufferedReader Breader = new BufferedReader(streamReader)) {
                String line;
                while ((line = Breader.readLine()) != null) {
                    templista.add(line);
                }
            }
            if (templista.size() == 2) {
                String tempUserName = templista.get(0);
                String tempPassCode = templista.get(1);
                String tempPassDecr = decrypt(hexStringToByteArray(tempPassCode), tempUserName);
                if (loginProvera(tempUserName, tempPassDecr)) {
                    return true;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static void upisCookija(String user, String pass) {
        try {
            File file = new File("src/main/resources/idhash.txt");
            try (PrintWriter writer = new PrintWriter(file)) {
                writer.write(user + "\n");
                writer.write(byteArrayToHexString(encrypt(pass, user)));
                writer.flush();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean loginProvera(String email, String sifra) {

        if (email.isEmpty()) {
            Dialog unesiteUsername = new Dialog();
            unesiteUsername.setContentText("Morate uneti email!");
            Button okisha = new Button("Ok");
            okisha.setOnAction(e2 -> {
                unesiteUsername.hide();
                emailTF.requestFocus();
            });
            unesiteUsername.getButtons().add(okisha);
            unesiteUsername.showAndWait();
        }
        if (sifra.isEmpty()) {
            Dialog unesitePass = new Dialog();
            unesitePass.setContentText("Morate uneti sifru!");
            Button okisha2 = new Button("Ok");
            okisha2.setOnAction(e2 -> {
                unesitePass.hide();
                passwordTF.requestFocus();
            });
            unesitePass.getButtons().add(okisha2);
            unesitePass.showAndWait();
        }
        if (sifra.equals(vratiSifruIzBaze(email))) {
            vratiNindzuPoImenu(email);
            return true;
        }
        return false;
    }
}
